/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
	"Product Sans:size=10",
	"Font Awesome 6 Free:style=Solid:size=7"
};
static const char *prompt = NULL;      /* -p  option; prompt to the left of input field */
static const char col_normfg[]      = "#D4BE98";
static const char col_normbg[]      = "#32302F";
static const char col_selfg[]       = "#32302F";
static const char col_selbg[]       = "#A9B665";
static const char *colors[SchemeLast][2] = {
	/*		 fg		bg		*/
	[SchemeNorm] = { col_normfg,	col_normbg	},
	[SchemeSel]  = { col_selfg,	col_selbg	},
	[SchemeOut]  = { "#000000",	"#00FFFF"	},
};
/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines = 0;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/* Size of the window border */
static unsigned int border_width = 0;
